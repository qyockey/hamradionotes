# 2.1 Radio Signals and Waves

### Frequency and Phase

- Radio waves are sinusoidal
- Frequency (f) in hertz measures cycles per sec
- Period (T) measures time per cycle (equal to $\frac{1}{f}$)
- Phase (no symbol) is measured in degrees $[0,360)$, used to comare how signals are aligned in time

### The Radio Spectrum

- AF: Audio frequency (20 Hz - 20 kHz)
- RF: Radio frequency (20 kHz - 300+ GHz)

+ AM band: 535 to 1700 kHz
+ FM band: 88 to 108 MHz

- VLF: 3 kHz - 30 kHz
- LF: 30 kHz - 300 kHz
- MF: 300 kHz - 3 MHz
- HF: 3 MHz - 30 MHz
- VHF: 30 MHz - 300 MHz
- UHF: 300 MHz - 3 GHz
- SHF: 3 GHz - 30 GHz
- EHF: 30 GHz - 300 GHz

order: VUSE

hams typically operate within MF to UHF

### Wavelength

- Wavelength ($\lambda$) measures distance travelled during one complete cycle or distance between peaks
- Waves travel at speed of light ($3 \cdot 10^8 \frac{m}{s}$)
- $\lambda = \frac{c}{f}$
- $\lambda \mathrm{\ in\ meters} = \frac{300}{f \mathrm{\ in\ MHz}}$
- ex: $f = 1$ MHz, $\lambda = \frac{300}{1 \mathrm{\ MHz}} = 300 \mathrm{m}$

# 2.2 Radio Equipment Basics

### Basic Station Organization

Three basic components:
- Transmitter (XMTR) generates signal that carries data (speech, mose code, etc.)
- Receiver (RCVR) recovers data from signal
- Antenna turns radio signals into energy (waves) and turns radio waves into signals

Transceiver (XCVR or "rig") combines the functionality of transmitter and receiver

Feed line connects antenna with transmitter and receiver and a TR Switch allows both components to share the same antenna

### Repeaters

- Re-transmit information on one signal onto another frequency
- Contain trasmitter and receiver, but use a *duplexer* instead of a TR Switch to re-transmit instantaneously

