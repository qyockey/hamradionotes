# 3.1 Electricity

### Current and Voltage

Current (I): flow of electrons through a circuit over time
- Measured in amperes (A), or $\frac{C}{s}$.
- Measured using an ammeter connected in series with a component.
- direct current (dc) flows only in one direction.
- alternating current (ac) regularly changes direction like a sine wave.

Voltage (E): electromotive force, or electric potential, that makes electrons move.
- Measured in volts (V), $\frac{J}{C}$, $\frac{W}{A}$, or $\frac{kg\ \cdot\ m^2}{s^2\ \cdot\ C}$.
- Measured using a voltmeter connected in parallel across a circuit.
- dc voltage has same polarity at all times (ex batteries)
- ac voltage regularly reverses polarity (ex household power, radio signal)

Voltage acts like pressure in water pipe, current acts like water flow.

### Circuits
- series: components are connected sequentially. Current is the same for all components. Voltage is divided across components.
- parallel: components have direct connection to positive current and ground without having to go through other components. Voltage is the same for all components, equal to that of the power source.
- open: caused by a break in any connection.
- short: caused by unintentional connection between two points in a circuit.

### Resistance and Ohm's law
- Resistance \(R\): how much a given object resists flow of electrons through it. Measured in Ω $\left(\frac{V}{A}\right)$ using a ohmmeter connected in parallel with a component.
- Conductors (ex metals, especially copper) allow electrons to flow easily in response to applied voltage.
- Resistors resist flow of electrons (ex wood, glass, ceramics, other non-metals).
- $I=\frac{E}{R}\ \ \ \ R=\frac{E}{I}\ \ \ \ \ E=I \cdot R$

### Power

- Power \(P\): rate at which electrical energy is used. Consumed in *loads*.
- Measured in watts (W), or $\frac{kg\ \cdot\ m^2}{s^3}$
- $P=I \cdot E\ \ \ \ E=\frac{P}{I}\ \ \ \ I=\frac{P}{E}$

### Multimeter
- Combines functionality of ammeter, voltmeter, and ohmmeter.
- Only measure what the dial is set to. Trying to measure voltage when set to measure resistance can damage it.
- Measuring voltages higher than what a multimeter is rated for can cause "flashover," damaging it, circuit components, or even you.
- If measuring resistance and reading starts low but gradually increases, that indicates the ohmmeter is connected across a large, discharged capacitor

# 3.2 Components and units

### Basic Components

Resistors
- Restrict flow of current through a circuit (ac or dc).
- Resistance is measured in ohms.

Capacitors
- Store energy in an electrical field.
- Created by a voltage between two conducting surfaces with an insulator called a *dielectric* in between.
- Capacitance is measured in farads (F), or $\frac{C}{V}$.

Inductors
- Store energy in a magnetic field.
- Created by a current flowing in a wire. 
- Inductance is measured in henrys (H).
- Made from wire coiled around a magnetic core. *(electromagnet?)*

All basic components are available in adjustable models for ease of tuning circuits. Variable resistors are also known as ***potentiometers***

Transformers
- made from two+ inductors that share energy
- can change combination of voltage/current outputted
- ex: change 120 V ac household power to lower voltage for appliances

### Reactance & Impedence

Reactance (X)
- Measures opposition to ac current flow
- Measured in ohms
- Change in ac current happens before change in voltage in a capacitor
- Change in ac current happens after change in voltage in an inductor

Impedence (Z)
- Combination of resistance and reactance
- Measures total opposition to ac current flow in a circuit
- Measured in ohms

### Resonance

- At resonant frequency (Hz), capacitive and inductive resistance cancel out
- Circuits with both capacitor(s) and inductor(s) are *resonant* or *tuned*

### Diodes, Transistors, and Integrated Circuits
Semiconductors:
- Materials that don't conduct as well as metallic conductors but aren't good insulators either
- Adding impurities can make P-type or N-type material
- When P and N are in contact, a *PN Juction* is formed, wich allows current to flow easier in one direction than the other

Diodes:
- Semiconductors that only allow current flow in one direction
- Diode that can handle high voltages is called a *rectifier*
- With an ac voltage applied, a pulsing dc current is output b/c only one direction of voltage can pass through
- Has two electrodes: anode and cathode. Cathode is identified by a stripe
- Forward voltage drop, usually < 1V, develops between anode and cathode when current flows through the diode
- LED (light emitting diode) emit light when current flows forward from anode to cathode

Transistors:
- Made of three layers of semiconductive material
- Use small voltages and currents to control larger ones, and can act as electronic switches
- Using a small signal to amplify or control larger ones is called *gain*
- Two main types:
  - Bipolar Junction Transistor (BJT): (base, collecter, & emitter)
  - Field Effect Transistor (FET): (gate, drain, & source)

Integrated Circuits contain many components connected together

### Protective Components

- Fuses and circuit breakers prevent damage to circuit components in case of an overload by cutting power
- Do not replace fuses or circuit breakers with ones of higher rating b/c a higher allowed current could damage components or start a fire
- Once a given current is reached, the metal strip in a fuse breaks and it cannot be reused
- When current overload occurs, circuit breakers trip, and can be reset to close the circuit again

### Circuit Gatekeepers
- *Switches* and *relays* control current by opening and closing paths for current to follow
- Switches are operated manually while relays are controlled by electormagnets
- Described in terms of *poles* (number of currents contrrolled) and *throws* (number of paths controlled).  ex: SPDT = single pole double throw, routes one current to one of two different paths

### Shematics and Component Symbols
See pages 3-15 to 3-17 of handbook

# 3.3 Radio Circuits

### Oscilators and Amplifiers
- Oscilator: generates a steady signal at a given frequency
- Driver: allows oscilator to run steadily at low power
- Power Amplifier: amplifies driver signal enough to communicate with radio stations
- Telegraph Key: keys driver and power amplifier stages (turns them on and off) 

### Modulators

- A modulator is used to add voice data to a radio signal
- A demodulator takes radio signal and extracts information

### Mixers

- Combine two radio signals and shift one of them to a third frequency (not to be confused with audio mixers)
- Used in both transmitters and receivers
