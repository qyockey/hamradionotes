# Ch 7

## Licensing Terms

### Part 97

- rules governing amateur radio are defined in part 97 of fcc rules
- one of the main purposes of amateur radio is to advance skills in the technical and communications phases of the radio art

### Type and Classes of Licenses

- amateur radio license consists of operator and station licenses
- each person can have only one combines operator/primary station license
- three available licenses rn are: technician, general, and amateur extra
- other deprecated licenses are: novice, technician plus, and advanced
- clubs can also hold licenses, given to *trustee* who serves as officer
- clubs must have at least four members

### Examinations

- upon passing exam, you must wait until your info shows up in fcc uls database before beginning to operate
- find your call sign in database

### Terms of License and Renewal

- amateur licenses are good for 10 years
- can renew indefinitely without taking the exam during those 10 years or during the following 2 year grace period
- don't transmit after license expires

### Your Responsibilities

#### Personal Information

- license can be revoked if you don't maintain a valid mail and email address

#### Station Inspection

- obligated to make station available for inspection upon request by fcc
- keep original license for inspection

## 7.2 Bands and Privileges

Technician band privileges:

#### VHF

- 6 meters: 50 - 54
- 2 meters: 144 - 148
- 1.25 meters: 219 - 220
- 1.25 meters: 222 - 225

#### UHF

- 70 cm: 420 - 450
- 33 cm: 902 - 928
- 23 cm: 1240 - 1300
- 13 cm: 2300 - 2310
- 13 cm: 2390 - 2450

#### HF

(200 W PEP max output)
- 80 m: 3.525 - 3.600 (CW only)
- 40 m: 7.025 - 7.125 (CW only)
- 15 m: 21.025 - 21.200 (CW only)
- 10 m: 28.000 - 28.300 (CW, RTTY, and data)
- 10 m: 28.300 - 28.500 (CW and SSB)

### Emission Privileges

- emission is formal name for radio signal by transmitter
- within most ham bands, there are restrictions by emission type
- bottom 100 kHz sub-bands of 6 m and 2 m are mode-restricted CW only
- 1.25 m from 219 - 220 MHz is mode-restricted for digital message forwarding
- no mode restrictions above 222 MHz
- *beacons* are stations that make transmissions for the purpose of observing propagation or other experiments
- beacons can be found on 10 m between 28.2 and 28.3 MHz and in lower segments of VHF, UHF, and microwave bands

### Power Limits

- output power from transmitter or amplifier is defined by *peak envelope power* (PEP)
- PEP is average power during one RF cycle
- above 30 MHz, amateurs can use up to full legal limit of 1500 W
- below 30 MHz, technicians are limited to 200 W

### Primary and Secondary Allocations

- parts of the radio spectrum are allocated to multiple parties
- secondary party may not harmfully interfere with a primary party in shared allocations
- ex: on 70 cm hams have a secondary allocation and may find radiolocation stations transmitting there

### Repeater Coordination

- FCC isn't in charge of repeater frequencies
- ham frequency coordinators recommend transmit and receive frequencies
- frequency coordinators are selected by local or regional amateurs whose stations are eligible to be auxiliary or repeater stations

## 7.3 International Rules

FCC is domestic only, international coordination is done through the *International Telecommunication Union* (ITU) and *International Amateur Radio Union* (IARU)

### Permitted Contacts and Communications

- international communication is permitted if it is limited to the purposes of amateur radio or remarks of a personal nature
- a countries government can notify the ITU that it objects to comms between its citizens and citizens of other countries, but this is very rare

### International Operating

- amateurs can transmit under an FCC license in international waters if on a vessel documented in the US
- otherwise, a reciprocal operating agreement must be formed with the US and the foreign country

## 7.4 Call Signs

- composed of prefix and suffix
- prefix is 1 or 2 letters followed by a number
- suffix is 1 to 3 letters
- US prefixes start with K, N, W, or two letter combo from AA to AL
- number in call sign corresponds to call district in which an amateur lives

