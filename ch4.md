# 4.1 Propagation

- Radio waves spread out from an antenna in straight lines like light
- Eventually becomes too weak to be received by spreading out too much or something in tiits path interferes with it
- Max distance a transmission can be received is *range*
- Obstructions create *shadowing*, especially when the signals are too high frequency to easily pass through objects
- Vegetation can completely absorb VHF and UHF waves, so range is typically higher in winter
- Fog and rain can absorb UHF and high VHF waves but doesn't affect HF and lower
- *Knife-edge diffraction* can allow waves to travel past large objects
- *Refraction* bends VHF and UHF waves in the atmosphere, allowing signals to travel past the horizon
- Signals can take different paths to the same destination, but may have different phases, which can weaken the signal or cancel it out completely. This is called *multipath*
- Multipath "dead spots" are around $\frac{\lambda}{2}$ apart, so moving your antenna even just a few feet can solve this issue
- Similarly, a station in motion transmitting at VHF or UHF can have a rapidly varying strength, called *picket-fencing*
- Multipath distortion can cause high error rate in reception of VHF and UHF signals, even though signal may be strong
- Temperature inversions in the atmosphere form *ducts* that can carry signals over 300 miles. Transmission with the help of the atmosphere is called *tropospheric propagation*, or just "tropo"

### The Ionosphere

- Located between lower atmosphere with dense air and space with no air at all is the ionosphere from about 30 - 260 miles above the Earth's surface
- Because of UV radiation, ionosphere is weakly conductive
- Divided into levels D, E, F1, & F2. D is closest to surface of Earth, F2 is closest to space
- Depending on solar activity and whether it is night or day, layers can refract (E, F1, F2) or absorb (D, E) signals
- Refracting off E and F layers is called *sky wave propagation* or *skip*
- Because Earth's surface is conductive too, waves can be reflected between ionosphere and surface multiple times and can allow signals to be received hundreds or thousands of miles away. Each bounce is called a *hop*
- Low frequencies get absorbed by atmosphere
- VHF and higher frequencies usually pass through atmosphere with little bending and become lost to space, generally need repeaters to be heard beyond the horizon
- High solar activity increases ionization of the ionosphere, and upper HF bands can be be reflected in the F layers
- Any point in the solar cycle, patches of E layer can occasionally become sufficiently ionized to refract 10, 6, and sometimes even 2 meter signals
- The aurora (northern lights) are moving charged particles that can reflect VHF signals, but are in constant motion and thus cause distortion
- Some dumbass figured out you can reflect a signal off of the *fuckin trail of a meteorite* for the couple seconds before it disappears. 6 meters is optimal

# 4.2 Antenna and Radio Wave Basics

- Antenna generates electromagnetic wave consisting of electric and magnetic fields perpendicular to each other that both oscillate with the same RF frequency in the antenna.
- Polarization is the orientation of the radio wave's electric field
- If polarization of transmitting antenna is different from polarization of receiving antenna, signal strength will be dramatically reduced, so be sure to align your antenna with the antenna of the receiving station
- Travelling through the ionosphere changes a waves polarization back and forth between horizontal and vertical, called *elliptical polarization*. This means that antenna used for transmitting and receiving on HF bands where skip propagation is common can be either horizontal or vertical and they will still pick up a signal

### Antenna Gain

- *Antenna gain* is the ability of an antenna to concentrate signals in a specific direction, increasing signal strength in that direction relataive to a reference antenna

### Decibels

- Represent exponential increases in power or voltage
- $\mathrm{dB} = 10 \log\left(\frac{P_f}{P_0}\right) = 10 \log\left(\mathrm{power\ ratio}\right)$
- $\mathrm{dB} = 20 \log\left(\frac{E_f}{E_0}\right) = 20 \log\left(\mathrm{voltage\ ratio}\right)$
- ex: power increase from 5 watts to 10 watts $=10\log\left(\frac{10\mathrm{\ W}}{5\mathrm{\ W}}\right) = 10 \log\left(2\right) \approx 3 \mathrm{\ dB}$
- ex: voltage decrease from 2 volts to 0.1 volts $=20\log\left(\frac{0.1\mathrm{\ V}}{2\mathrm{\ V}}\right) = 20 \log\left(0.05\right) \approx -26 \mathrm{\ dB}$

# 4.3 Feed Lines and SWR

- *Feed line loss* is the converting of power passing through a feed line into heat. Loss increases with frequency

### Coaxial Cable

- Most common feed line used by hams is the coaxial cable because of its ease of use
- Hardline coax is surrounded by a shield of aluminum or copper which makes it stiff but drastically reduces loss

### Characteristic Impedence

- A measurement of how energy is carried through the feed line
- Typically denoted as $Z_0$
- Most ham coax has a characteristic impedance of 50 $\Omega$

### Standing Wave Ratio - SWR

- Power is carried by feed line to a load (like an antenna)
- If load and feed line impedances do not match, some power is reflected by the load
- Power travelling towards the load is *forward power*, power reflected by load is *reflected power*
- The higher the impedance difference, the more forward power is reflected
- Power travelling in opposite directions along the feed line creates standing waves
- Standing Wave Ratio (SWR) is the ratio of the maximum value (loop) to the minimum value (node) of the standing wave
- Measured using an SWR meter, usually built into tranceiver
- If using an external SWR meter, ensure it is specified for the frequency range in use so it will make accurate measurements
- Indicates how well feed line and load impedances are matched
- Also can be calculated by ratio of antenna-to-feed-line or feed-line-to-antenna impedances, wichever is greater than 1
- *Perfect match* occurs at SWR of 1:1
- SWR > 1 is called *impedance mismatch*
- Because an antenna's impedance changes with frequency, SWR also changes with frequency
- Low SWR indicates efficient transfer of power from feed line, which means less signal loss
- Once SWR crosses a certain threshold, usually 2:1, the transmitter reduces power to avoid damaging the output transistors
- Antenna too long or short for the frequency being used can cause high SWR
- Faulty feed line or feed line connectors also increases SWR
- Erratic SWR usually means a loose connection in feed line or antenna

# 4.4 Practical Antenna Systems

### Dipoles and Ground Planes

- A *dipole* is the simplest type of antenna and easy to use, made from a straight segment of wire $\frac{1}{2}\lambda$ in length with a feed connector in the middle
- Most are oriented horizontally, especially on lower frequencies, and emit a horizontally polarized signal
- Signal radiates strongest broadside to the antenna

+ *Ground planes* are another popular antenna choice
+ Typically $\frac{1}{4}\lambda$ in length with feed point at base of antenna
+ Acts like half of a dipole with the ground plane acting as an electrical mirror as the other half
+ Ground plane is usually sheet metal or a screen of wires called *radials*
+ $\frac{5}{8}\lambda$ ground plane antenna focuses more energy to horizon because of its extended length, increasing its range
+ Coiling some of the wire in the antenna or adding an inductor, called *inductive loading*, makes the antenna longer electrically than it is physically
+ Generally orinted vertically
+ Signal is strongest broadside to the antenna, so signal strength is equal in all directions

### Anteanna Length

- For dipoles: $l \mathrm{\ (feet)} = \frac{468}{f \mathrm{\ (MHz)}}$
- Because ground plane antennas are half as long, formula outputs half the length of the dipole formula
- For ground planes: $l \mathrm{\ (feet)} = \frac{234}{f \mathrm{\ (MHz)}}$
- ex: for ground plane operating at 146 MHz, length = $\frac{468}{146}$ = 1.6 feet = 19.25 inches
- When constructing your own antenna, cut the wire several percent longer than necessary. Resonant frequency will likely be too low, so use an SWR meter or antenna analyzer to cut the wire to the right length to increase the resonant freqency

### Handheld Radio Antennas

- Short and flexible antenna is called a *rubber duck*
- Size is convenient, but is weaker and less efficient than a standard ground plane
- When using inside of a vehicle, the metal sides act as shields that dramatically reduce signal strength

### Directional Antennas

- *Beam antennas* concentrate signals in one direction
- Most common type of beam antenna used by hams are *Yagis*
- Offer high gain in a specific direction
- Horizontally polarized Yagis are usually used for long distance communications, expecially for SSB and CW contacts on VHF and UHF ***(what does SSB or CW mean?)***
- At higher than 1 GHZ, constructing antennas is difficult so dish antennas are typically used instead

## Practical Feed Lines and Associated Equipment

### Feed Line Selection and Maintainance

- See manual page 4-17 for table comparing popular ham coax cables
- Generally, RG-213 cables have less loss than RG-58
- Damage to outer layer of coax can allow for mosture contamination, the most common source of damage
- UV exposure can damage jacket

### Coaxial Feed Line Connectors

- Coax connectors are used to make connections between coax cable and radios, accessory equipment, and antennas
- UHF series of connectors (not ultra-high frequency) PL-258 plugs and SO-239 receptacles and the most widely-used on HF equipment
- Above 400 MHz, Type N connectors are used
- Overlap between UHF and N on 6, 2, and 1.25 meter equipment
- Faulty connections introduce loss and must  be watertight
- If using air core coax, special techniques must be used to prevent water intrusion

### Soldering

- Used to install most coax cables
- Best solder to use for radio and electronics is rosin-core
- Do not use acid-core solder
- Cold tin-lead solder joints have rough/lumpy surface

### SWR Meters and Wattmeters

- SWR is measured with an SWR meter or directional wattmeter
- Meter is placed in series with feed line

### Antenna Tuners

- Antenna tuners are used to match transmitter output impedance to antenna system impedance (SWR of 1:1)
- Allow circuit to run at full power without risking damaging output transistors

### Antenna Analyzers

- Measure many helpful variables, such as SWR, impedance, frequency, and can tell if an antenna is resonant at a desired frequency

