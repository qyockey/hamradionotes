# Chapter 8: Operating Regulations

## 8.1 Control Operators

- *control operator*
  - licensed operator responsible for making sure all FCC rules are followed
  - not necessarily the same as station owner
  - station licensee is responsible for delegating control operator
  - must appear in FCC amateur license database
  - all transmissions must be made under supervision of control operator
- *control point*
  - wherever the control operator operates the transmitter
  - can be remotely located and connected by phone lines, internet, or radio link

### Privileges and Guest Operating

- control operator may operate station in any way permitted by license class
- guest privileges are restricted by license class of supervising host
- both guest and host are responsible for proper use of station
- FCC will presume station licensee to be station operator unless a written record states otherwise

## 8.2 Identification

- unidentified transmissions are only allowed when controlling model craft
- give call sign at start and end of transmission and at least once every 10 minutes
- call sign must be given in English if using phone, even if communicating in another language
- phonetic alphabet is recommended, but not required
- id can be given in cw even if using phone

### Tactical Calls

- when using tactical call signs (locations or similar, used in emergencies), FCC call sign still must be repeated every 10 minutes

### Self-Assigned Indicators

- "stroke", "slash", "slant", and "portable" are all acceptable identifiers to inform others that you are operating from a different location

### Test Transmissions

- when testing you must give call sign
- call sign + "testing" or call sign + "VVV"

## 8.3 Interference

### Harmful and Willful Interference

- *harmful interference*: transmission degrades, obstructs, or repeatedly interrupts communications
- when interference happens, politely negotiate with the other station, and one station should change frequency
- intentionally causing harmful interference is called *willful interference* and is never allowed

## 8.4 Third-Party Communications

- relaying of messages is third-party communications
- message from amateur station to another station on behalf of another person

### Definitions and Rules

- licensed amateur capable of being control operator of either station is not considered a third party
- any form of communication qualifies
- letting an unlicensed individual make contact under your supervision is third party communication
- third party can also be on receiving end of communication
- third party communications are only permitted with countries listed on page 8-8

## 8.5 Remote and Automatic Operation

### Definitions

- Local control: control operator is physically present
- Remote operation: operator is present at control point some distance away controlling station via some kind of control link (ex: internet link)
- Automatic operation: control operator is required but doesn't need to be at control point when station is transmitting. (ex: repeaters, satellites, digipeaters)

### Responsibilities

- if repeater violates FCC rules, it can be required to operate under remote control instead of automatic
- repeater users are responsible for complying with FCC rules when transmitting through a repeater
- if repeater inadvertently retransmits signal violating FCC rules, originating station is at fault

## 8.6 Prohibited Transmissions

- without identification
- deceptive (ex: using someone else's call sign)
- false distress or emergency signals
- obscene or indecent speech

### Business Communications

- all communications related to conducting your business or an employer's activities are prohibited
- it is okay to advertise amateur radio equipment for sale infrequently
- can't be paid for time operating amateur station
- only exception is teachers can use ham radio as part on instruction

### Encryption

- translating information into data is called encoding
- going from data to information is decoding
- reducing message size to transmit more efficiently is compression
- encoding and compression are allowed as long as protocol is publicly available
- encryption is encoding designed to hide meaning of message
- encryption is only allowed when transmitting to radio craft and space stations or where interception could have serious consequences

### Broadcasting and Retransmission

- broadcasting is one-way transmission intended for the general public
- broadcasts are prohibited except for transmitting code practice, information bulletins, or when necessary for emergency communications
- hams are prohibited from assisting with news gathering by broadcasting organizations
- music can only be transmitted as part of authorized rebroadcast of space station transmissions
- retransmitting of another station's signals is also generally prohibited, except when relaying a message or data

