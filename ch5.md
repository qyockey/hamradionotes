# 5.1 Modulation

Modulation gives meaning to a simple radio signal my changing various physical features to encode it with information

### Bandwidth

- Range of frequency of a signal
- Ex: voice covers about 300 Hz to 3000 HZ, so bandwidth is 2700 Hz
- *Composite signals* are made up of many individual *component* signals

### Modulation

- Signal w/o information is *unmodulated*
- Recovering information from a signal is *demodulation*
- If signal is modulated w/ speech information, the resulting signal is *phone mode* or *voice mode*
- If signal is modulated w/ data, resulting signal is *data mode* or *digital mode*
- *Analog mode* carries information that can be directly understood by a human (speech, Morse code, etc.)
- Digital mode must be decoded by a computer
- The three properties of waves that can be modulated are amplitude, frequency, and phase

### Continuous Wave (CW)

- Simplest signal w/ amplitude and frequency that never change is a *continuous wave* (CW)
- Simplest type of modulation is turning a CW wave on and off in a coded pattern like Morse code

### Amplitude Modulation

- Varying signal strength to encode data is called *amplitude modulation*, or AM
- Recovering voice data from AM signal is called *detection*
- AM signal is made up of three parts: a *carrier* and two *sidebands*
- Carrier is a steady unmodulated signal
- Carrier is surrounded by upper sideband (USB) and lower sideband (LSB) which are copies of the same data

### Single-Sideband (SSB)

- Only one sideband is necessary to transmit data, so carrier and other sideband can be suppressed to concentrate signal and increase signal strength
- Below 10 MHz, LSB is used
- Above 10 MHz, USB is used - including all VHF and UHF bands
- Exception: on the five channels of the 60 meter band (5 MHz), USB use by hams is required

### Frequency and Phase Modulation

- Using frequency to modulate information into a signal is called *frequency modulation* or FM
- Modulating information by changing phase is called *phase modulation* or PM and is nearly identical to FM
- FM has excellent noise rejection qualities, so it is the mode used by many VHF and UHF repeaters
- B/c of the way FM receivers work, only one signal can be received at a time, called the *capture effect*

### Bandwidth of Modulated Signals

- SSB has half the bandwidth of AM and much lower bandwidth than FM so can be used in poorer conditions and has longer range
- Commonly used for long distance or weak signal contacts
- CW signals are even lower bandwidth and even better suited for weak signal communication

# 5.2 Transmitters and Receivers

### Selecting Band, Frequency, and More

- Set band *(tuning)* with button(s)
- Set frequency within band using VFO (variable frequency oscilator) knob or keypad
- On *multimode* radios, user can select between any of SSB (LSB or USB), AM, FM, CW, and data
- Use memory channels to store frequencies of channels for quick access

### Transmitter Functions

- Switching between receive and transmit can be done manually by grounding PTT (push to talk) or automatically with VOX (voice-operated control circuit)
- Hand keying uses a straight key to generate a CW signal manually
- An *electronic keyer* generates CW automatically much faster than a human can
- PEP (peak envelope power) measures AM or SSB signal's power when mic input is loudest
- CW, FM, and most digital modes have constant power output so PEP is constant
- A *dummy load* is a high-duty resistor used to absorb signals when testing to prevent accidentally transmitting
- When transmitting, all signal must be inside a single band. Ex: an FM signal is 15 kHz wide, 7.5 on each side of the center, so leave at least 10 kHz from edge of band to be safe

##### Spurious Signals

- Excessive modulation can cause undesired transmitter outputs on adjacent frequencies, called *splattering*
- Overmodulated FM signal with excessive deviation is called *overdeviation*, caused primarily by speaking too loudly into mic
- Overmodulation of AM or SSB signal is caused by speaking too loudly or mic gain is too high

### Receiver Functions

- *AF gain* is volume control knob
- HF rigs have *RF gain control* to adjust sensitivity to incoming signals
- *Attenuator* reduces strength of signals to prevent overloading the receiver
- *Automatic Gain Control (AGC)* circuit automatically adjusts receiver's sensitivity to keep output volume constant for both weak and strong signals
- *Squelch* circuit mutes receiver output when on signal is present
- Lowering squelch may be necessary to hear weak signals

##### Selectivity and Sensitivity

- Sensitivity
  - Determines receiver's ability to detect signals, specified as a *minimum detectable signal* measured in microvolts ($\mu V$)
  - Lower MDS means higher sensitivity
  - If receiver isn't sensitive enough, a *preamplifier* can be used, installed between receiver and antenna
- Selectivity
  - Ability of a receiver to discriminate between signals and retrieve only information from desired signal in the presence of other unwanted signals

##### Filtering and Tuning

- *Filters* are used to reject signals that are wider than the desired bandwidth
- Wide filters (about 2.4 kHz) are used for SSB reception, and narrow filters (around 500 Hz) are  used for CW signals and data mode
- If you have multiple filters available, you can select the desired filter for the signal you want to receive
- *Receiver incremental tuning (RIT)* or *clarifier* allows operator to tune receiver frequency without changing transmitter frequency, meaning you can adjust pitch of voice pitch that seems too high or too low
- Only useful for CW ad SSB, FM signal pitch doesn't get adjusted, only distorted

### VHF/UHF RF Power Amplifiers

- RF power amplifiers can be used to increase transciever output power
- Most amplifiers have a CW/SSB switch to change operating mode of power amplifier

### Transverters

- Take RF input on one band and output on another band with help of mixers

# 5.3 Digital Communications

- Digital or data modes combine modulation with a *protocol*, sets of rules by which data is packaged and exchanged
- Allows for much faster and more effiecient transfer of data, better with weak signal, and have error correction
- Some popular digital modes on HF for use with ionospheric (or skip) propagation:
  - Radioteletype (RTTY)
  - Keyboard-to-keyboard, ex PSK31
  - Weak-signal modes, ex ***FT8*** or WSPR
  - PACTOR or WINMOR for winlink system messaging
- Some popular digital modes on VHF and UHF:
  - ***Packet radio*** with AX.25 protocol
  - B2F protocol for winlink system messaging
  - JT65 for moonbounce and MSK144 for scatter paths
  - ***IEEE 802.11*** (Wi-Fi)
- Amateurs also use digital protocols to communicate by voice with *digital voice* modes
- On HF, ex AOR and FreeDV
- On VHF/UHF, ex D-STAR, SystemFusion (C4FM), DMR (Digital Mobile Raio), and P25
- Digital voice covered in more depth in ch6

##### IP-Based Microwave Networking

- Amateur radio mesh networks are peer-to-peer networks of modified commercial Wi-Fi equipment

##### The WSJT Modes

- WSJT software suite provides open-source software for special types of digital modes, including JT-65 for moonbounce, weak-signal propagation beacons (WSPR), and meteor scatter (MSK144)
- Their latest invention, FT8, a digital mode capable of low signal-to-noise operation

### Packet and Packet Networks

- Packet radio is most common digital mode on VHF and UHF
- Data characters are transmitted in packets containing header, data, and checksum
- Data is encoded with frequency-shift-keying (FSK), rapidly alternating audio tones
- Data is reassembled by modem and terminal node controller (TNC) on receiving end
- If checksum indicates errors in transmission were present, receiver automatically requests retransmission (ARQ or automatic repeat request)
- ARQ is part of AX.25 protocol to ensure transmissions are error-free

### Keyboard-to keyboard

- Digital modes designed for real-time person-to-person communication are called keyboard-to-keyboard modes, most popular on HF
- Radioteletype, invented in the 1930s was the first example
- Today, most popular is PSK31, or phase shift keying, 31 baud
- PSK31 has very low transmission rate, but works incredibly well in noisy conditions

### APRS

- Automatic Packet Reporting System uses packet radio to transmit info about location of portable stations using GPS
- Can also transmit weather info and even short text messages
- Primary use is to show locations of stations on computer-generated maps in real time

### Setting up for Digital Modes

- If TNC is used, it is connected to COM or USB ports on computer, and to mic in (for transmitting) and to spkr out (for receiving)
- If sound card is used instead, its output is connected to radio's mic in and radio's speaker out is connected to computer's audio in.
- Ex: with FT8, WSJT-X software runs on computer, audio tones for xmt and rcv are passed through sound card connections. 
- Computer "line in" to xcvr spkr connector

### Gateways

- Digital stations that connect other amateur stations to the internet
- All rules surrounding commercial use still apply

# 5.4 Power Supplies and Batteries

### Power Supplies

- Convert household ac power to dc for radio
- Make sure power supply has correct output voltage for radio
- If output voltage is 12V, the 13.8V from a car battery should also work fine
- Make sure power supply current is greater than max current radio can draw
- If radio manual doesn't list a current, $P\geq2\cdot\frac{\mathrm{output\ power}}{\mathrm{input\ voltage}}$
- Ex for 50W 13.8V radio: $\mathrm{power\ supply\ current} \geq 2\cdot\frac{50\ W}{13.8\ V} \geq 7.2\ A$, so any supply with 8 A or more should work well
- Supply's output voltage changes with amount of output current, so a *regulator circuit* is used to minimize voltage change
- Percentage of voltage change between no load (zero current) and full load (max current) is the *regulation* of the supply

#### Wire Size and Power Sources

- If wires between power source and transceiver are thin or long, the added resistance in the wires will cause voltage drop when drawing large currents

### Mobile Power Wiring

- Connect radio's negative lead to ground on car battery

### Batteries

- Alkaline, carbon-zinc, and lithium batteries are non-rechargeable
- Nickel-cadmium, nickel-metal hydride, lithium-ion, and lead acid batteries are rechargeable
- To find how long a battery will last, divide ampere-hours of battery by average current draw by equipment
- Charging batteries too fast can result in overheating or releasing of explosive hydrogen gas
